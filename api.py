from flask import Flask, jsonify

app = Flask(__name__)

books = [
    {
        'id': 1,
        'titulo': 'O Pequeno Príncipe',
        'autor': 'Antoine de Saint-Exupéry'
    },
    {
        'id': 2,
        'titulo': 'Harry Potter e a Pedra Filosofal',
        'autor': 'J.K. Rowling'
    },
    {
        'id': 3,
        'titulo': 'O Senhor dos Anéis',
        'autor': 'J.R.R. Tolkien'
    }
]


@app.route('/books/<int:id>', methods=['GET'])
def get_book(id):

    book = next((book for book in books if book['id'] == id), None)

    if book is None:
        return jsonify({'error': 'Livro não encontrado'}), 404

    return jsonify(book), 200

if __name__ == '__main__':
    app.run()